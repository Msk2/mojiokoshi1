function doPost(e) {  
  const LINE_ACCESS_TOKEN = 'Aj1/F6GuqGQwqify0G2IjxQulW9Z+Gd7qce9qfG2Ad+blfL3m4M8zq4TUF3J4/uRB8ASQjd9iKz7GWY/2o+hQur3T9uN22nTvfZ8xfQwXmrGU+SBtYtKlSCDL07C3VfVJ/TMo0ZFhA+T41X/oPAfTgdB04t89/1O/w1cDnyilFU=';
  const lineJson = JSON.parse(e.postData.contents);  
  const replyToken = lineJson.events[0].replyToken;  
  const messageId = lineJson.events[0].message.id;  
  const lineImageUrl = "https://api.line.me/v2/bot/message/" + messageId + "/content/";  
  const lineImageResponse = UrlFetchApp.fetch(lineImageUrl, {    
    'headers': {      
      'Content-Type': 'application/json; charset=UTF-8',      
      "Authorization": "Bearer " + LINE_ACCESS_TOKEN    
    },    
    'method': 'get'  })  
  .getContent();  
  const lineImageBlob = Utilities.base64Encode(lineImageResponse);  
  const API_KEY = 'v6uCrNwYHGVoLqd2tbMf19+TR63Z6pt5wmxLz+CN0MF8MciAbgGzhXzCQvm42qvWB8ASQjd9iKz7GWY/2o+hQur3T9uN22nTvfZ8xfQwXmpz/ZMOjx3F8pk+a1uJaJ08lCFWk29m8OJM8wu1yFIbuwdB04t89/1O/w1cDnyilFU=';
  const visionRequestUrl = 'https://vision.googleapis.com/v1/ images:annotate?key=' + API_KEY;  
  const payload = JSON.stringify({      
    "requests":[        
      {          
        "image": {            
          "content": lineImageBlob          
        },          
        "features": [            
          {              
            "type": "TEXT_DETECTION",
            "maxResults": 1            
          }          
        ]        
      }      
    ]    
  });  
  const visionTextResponse = UrlFetchApp.fetch(visionRequestUrl, {    
    method: "POST",    
    contentType: "application/json",    
    payload: payload,  
  })  
  .getContentText();  
  const visionTextJson = JSON.parse(visionTextResponse);  
  const visionText = visionTextJson.responses[0].fullTextAnnotation. text;  
  const messages = [    
    {
      "type":"text",      
      "text": visionText    
    }  
  ]  
  UrlFetchApp.fetch("https://api.line.me/v2/bot/message/reply", {    
    "headers": {      
      "Content-Type": "application/json; charset=UTF-8",      
      "Authorization": "Bearer " + LINE_ACCESS_TOKEN,    
    },    
    "method": "post",    
    "payload": JSON.stringify({      
      "replyToken": replyToken,      
      "messages": messages,    
    })  
  }); 
}